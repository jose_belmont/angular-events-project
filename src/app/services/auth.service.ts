import { OkResponse } from '../interfaces/responses';
import { HttpClient } from '@angular/common/http';
import { EventEmitter, Injectable } from '@angular/core';
import { SERVER } from '../app.constants';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/of';
import { HttpErrorResponse } from '@angular/common/http/src/response';
import { Observable } from 'rxjs/Observable';
import { IUser } from '../auth/interfaces/i-user';

@Injectable()
export class AuthService {
    logged: boolean;
    $loginEmitter: EventEmitter<boolean> = new EventEmitter<boolean>();
    private user: IUser;

    constructor(private http: HttpClient) { }

    login(email: string, password: string, lat: number, lng: number): Observable<boolean> {
        const dataLogin = {
            email: email,
            password: password,
            lat: lat,
            lng: lng
        };
        return this.http.post<{ token: string, ok: boolean }>(`${SERVER}auth/login`, JSON.stringify(dataLogin))
            .map(response => {
                if (response.ok) {
                    localStorage.setItem('token', response.token);
                    this.logged = true;
                    this.$loginEmitter.emit(true);
                    return true;
                }
                return false;
            });
    }

    isLogged(): Observable<boolean> {
        if (this.logged) {
            return Observable.of(true);
        } else if (!this.logged && !localStorage.getItem('token')) {
            return Observable.of(false);
        } else {
            return this.http
                .get(`${SERVER}auth/token`)
                .catch((resp: HttpErrorResponse) => Observable.throw('Error with token service'))
                .map((resp: OkResponse) => {
                    if (!resp.error) {
                        this.logged = true;
                        this.$loginEmitter.emit(true);
                        return true;
                    } else {
                        throw resp.error;
                    }
                });
        }
    }

    loginGoogle(user: IUser, token: string): Observable<boolean> {
        console.log('respgoogle:', token);
        localStorage.setItem('token', token);
        return this.http.get(`${SERVER}auth/google`)
            .catch((error: HttpErrorResponse) => Observable.throw(`Error trying to login google. Server returned ${error.message}`))
            .map((resp: OkResponse) => {
                if (!resp.error) {
                    localStorage.setItem('token', resp.token);
                    this.user = resp.result;
                    this.logged = true;
                    this.$loginEmitter.emit(true);
                    return true;
                }
                throw resp.errorMessage;
            });
    }

    loginFacebook(user: IUser, respFacebook: any): Observable<boolean> {
        console.log('respFacebook:', respFacebook);
        localStorage.setItem('token', respFacebook);
        return this.http.get(`${SERVER}auth/facebook`)
            .catch((error: HttpErrorResponse) => Observable.throw(`Error trying to login google. Server returned ${error.message}`))
            .map((resp: OkResponse) => {

                if (!resp.error) {
                    localStorage.setItem('token', resp.token);
                    this.user = resp.result;
                    this.logged = true;
                    this.$loginEmitter.emit(true);
                    return true;
                }
                throw resp.errorMessage;
            });
    }

    logout() {
        localStorage.removeItem('token');
        this.logged = false;
        this.$loginEmitter.emit(false);
    }
}
