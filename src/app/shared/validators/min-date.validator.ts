import { Directive, Input } from '@angular/core';
import { Validator, AbstractControl, NG_VALIDATORS } from '@angular/forms';

@Directive({
    selector: '[aeMinDate]',
    providers: [{provide: NG_VALIDATORS, useExisting: MinDateDirective, multi: true}]
})
export class MinDateDirective implements Validator {
    @Input() aeMinDate: string;

    validate(c: AbstractControl): { [key: string]: any } {
        if (c.value) {
            const minDate   = new Date(this.aeMinDate);
            const inputDate = new Date(c.value);
            return minDate <= inputDate ? null : {'minDate': minDate.toLocaleDateString()};
        }
        return null;
    }

    constructor() {}
}
