import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MinDateDirective } from './validators/min-date.validator';
import { GmapsAutocompleteDirective } from './directives/gmaps-autocomplete.directive';
import { ConfirmModalComponent } from './confirm-modal/confirm-modal.component';

@NgModule({
    imports: [
        CommonModule,
        NgbModule
    ],
    entryComponents: [
        ConfirmModalComponent
    ],
    declarations: [
        MinDateDirective,
        GmapsAutocompleteDirective,
        ConfirmModalComponent
    ],
    exports: [
        MinDateDirective,
        GmapsAutocompleteDirective,
        ConfirmModalComponent
    ]
})
export class SharedModule { }
