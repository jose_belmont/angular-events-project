import swal from 'sweetalert2';
import { ActivatedRoute, Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { IEvent } from '../interfaces/i-event';
import { EventService } from '../services/event.service';
import { IUser } from '../../auth/interfaces/i-user';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap/modal/modal';
import { ConfirmModalComponent } from '../../shared/confirm-modal/confirm-modal.component';


@Component({
    selector: 'ae-event-details',
    templateUrl: './event-details.component.html',
    styleUrls: ['./event-details.component.css']
})
export class EventDetailsComponent implements OnInit {
    activatedRoute: any;
    event: IEvent;
    users: IUser[];
    tickets = 1;
    total: number;
    payed: boolean;
    zoom = 17;
    payedMessage: string;
    paymentStatus: boolean;
    body = [];

    constructor(private route: ActivatedRoute,
        private router: Router,
        private eventService: EventService,
        private bootModal: NgbModal) { }

    ngOnInit() {
        this.event = this.route.snapshot.data['event'];
        this.resultBuy();
        this.eventService.listUserAttends(Number(this.route.snapshot.paramMap.get('id'))).subscribe(
            ok => this.users = ok,
            error => console.error(error)
        );
    }

    removeEvent() {
        this.eventService.removeEvent(this.event.id).subscribe(response => {
            this.router.navigate(['/events']);
        }, error => {
            console.error(error);
        });
    }

    resultBuy() {
        // Validate the possibility of 0 tickets
        this.tickets = this.tickets < 1 ? 1 : this.tickets;
        this.total = this.event.price * this.tickets;
    }
    sumTickets(x) {
        this.tickets += x;
        this.resultBuy();
    }

    getPayment(ok: boolean) {
        if (ok) {
            this.eventService.attendEvent(this.event.id, this.tickets).subscribe(res => {
                this.payedMessage = 'Payment correct!';
                this.paymentStatus = true;
                swal(
                    `The payment for ${this.event.title} is accepted`,
                    `You buy ${this.tickets} with total amount of ${this.total} €`,
                    'success'
                );
            }, error => {
                this.payedMessage = error;
                this.paymentStatus = false;
            });
        } else {
            swal(
                'Error',
                'Sorry, the payment is cancelled',
                'error'
            );
            this.payedMessage = 'Payment cancelled';
            this.paymentStatus = true;
        }

    }
}
