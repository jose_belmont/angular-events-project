import { GeolacationService } from './../../auth/services/geolacation.service';
import { EventService } from '../services/event.service';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { IEvent } from '../interfaces/i-event';
import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { NgModel } from '@angular/forms';
import { GoogleMap } from '@agm/core/services/google-maps-types';
import { IAddEvent } from '../interfaces/i-addEvent';


@Component({
    selector: 'ae-event-add',
    templateUrl: './event-add.component.html',
    styleUrls: ['./event-add.component.css']
})
export class EventAddComponent implements OnInit {

    @Output() eventAdded = new EventEmitter<IEvent>();
    newEvent: IAddEvent;
    editEvent: IEvent;
    error = false;
    errors = '';
    zoom = 17;
    today: string;
    edit: boolean;
    isSave = false;

    constructor(private eventService: EventService,
        private router: Router,
        private route: ActivatedRoute) { }

    ngOnInit() {
        this.resetNewEvent();
        const d = new Date();
        this.today = `${d.getFullYear()}-${d.getMonth() + 1}-${d.getDate()}`;
        if (this.route.snapshot.paramMap.get('id')) {
            this.edit = true;
            this.eventService.getEvent(Number(this.route.snapshot.paramMap.get('id'))).subscribe(
                event => {
                    this.newEvent = event;
                },
                error => alert(error)
            );
        } else {
            this.edit = false;
        }
    }

    resetNewEvent() {
        this.newEvent = {
            title: '',
            image: '',
            date: '',
            description: '',
            price: 0,
            lat: 38.4039418,
            lng: -0.5288701
        };
    }

    changeImage(fileInput: HTMLInputElement) {
        if (!fileInput.files || fileInput.files.length === 0) { return; }
        const reader: FileReader = new FileReader();
        reader.readAsDataURL(fileInput.files[0]);
        reader.addEventListener('loadend', e => {
            this.newEvent.image = reader.result;
        });
    }

    addEvent() {
        if (this.edit) {
            this.eventService.updateEvent(this.newEvent).subscribe(
                ok => this.router.navigate(['/events']),
                error => console.log(error),
                () => this.isSave = true
            );
        } else {
            this.eventService.addEvent(this.newEvent).subscribe(
                ok => this.router.navigate(['/events']),
                error => console.log(error),
                () => this.isSave = true
            );
        }
    }

    changePosition(pos: google.maps.places.PlaceResult) {
        this.newEvent.lat = pos.geometry.location.lat();
        this.newEvent.lng = pos.geometry.location.lng();
        this.newEvent.address = pos.formatted_address
    }

    validateClasses(ngModel: NgModel, validClass: string, errorClass: string) {
        return {
            [validClass]: ngModel.touched && ngModel.valid,
            [errorClass]: ngModel.touched && ngModel.invalid
        };
    }
}
