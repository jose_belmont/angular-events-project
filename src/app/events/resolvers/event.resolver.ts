import { EventService } from '../services/event.service';
import { Resolve, Router, ActivatedRouteSnapshot } from '@angular/router';
import { Injectable } from '@angular/core';
import { IEvent } from '../interfaces/i-event';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/of';

@Injectable()
export class EventResolver implements Resolve<IEvent> {
  constructor(private eventsService: EventService, private router: Router) {}

  resolve(route: ActivatedRouteSnapshot): Observable<IEvent> {
    return this.eventsService.getEvent(route.params['id'])
      .catch(error => {
        this.router.navigate(['/events']);
        return Observable.of(null);
      });
  }
}
