import { EventItemComponent } from './../event-item/event-item.component';
import { EventDetailsComponent } from './../event-details/event-details.component';

import { Observable } from 'rxjs/Observable';
import { EventService } from '../services/event.service';
import { IEvent } from '../interfaces/i-event';
import { Component, OnInit, NgZone } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Title } from '@angular/platform-browser';

@Component({
    selector: 'ae-events-show',
    templateUrl: './events-show.component.html',
    styleUrls: ['./events-show.component.css']
})

export class EventsShowComponent implements OnInit {

    ArrayEvents: IEvent[] = [];
    search = '';

    constructor(
        private eventService: EventService,
        private route: ActivatedRoute,
        private titleService: Title
    ) { }

    ngOnInit() {
        let typeShow: Observable<IEvent[]> = this.eventService.getEvents();
        switch (this.route.snapshot.data['show']) {
            case Show.assist:
                this.titleService.setTitle('Events to I assist');
                typeShow = this.eventService.getEventsAttend();
                break;

            case Show.mine:
                this.titleService.setTitle('My events');
                typeShow = this.eventService.getEventsMine();
                break;

            default:
                this.titleService.setTitle('All the Events');
                typeShow = this.eventService.getEvents();
                break;

            // this.eventService.getEvents().subscribe(
            //     events => this.events = events
            // );
        }
        typeShow.subscribe(
            // tslint:disable-next-line:whitespace
            events => {this.ArrayEvents = events; console.log(events);},
            (error: string) => console.log('Error loading events: ' + error),
        );
    }
    orderDate() {
        this.ArrayEvents.sort((e1, e2) => e1.date.localeCompare(e2.date));
        this.search = '';
    }

    orderPrice() {
        this.ArrayEvents.sort((e1, e2) => e1.price - e2.price);
        this.search = '';
    }

    removeEvent(index: number) {
        this.eventService.removeEvent(this.ArrayEvents[index].id).subscribe(response => {
            this.ArrayEvents.splice(index, 1);
            this.search = '';
        }, error => {
            error = 'Event has tickets buyed';
            console.error(error);
        });
    }
}
export enum Show {
    all,
    mine,
    assist
}
