import { IUser } from './../../auth/interfaces/i-user';
import { EventsResponse, EventResponse } from '../interfaces/responses';
import { OkResponse } from '../../interfaces/responses';
import { IEvent } from '../interfaces/i-event';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { SERVER, IMG_EVENTS_PATH, IMG_PROFILE_PATH } from '../../app.constants';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import { HttpErrorResponse } from '@angular/common/http/src/response';
import { IAddEvent } from '../interfaces/i-addEvent';


@Injectable()
export class EventService {

    constructor(private http: HttpClient) { }

    // Get all the events
    getEvents(): Observable<IEvent[]> {

        return this.http.get(`${SERVER}events`).map((response: { events: IEvent[], ok: boolean }) => {
            if (response.ok) {
                response.events.map(ev => {
                    ev.image = `${IMG_EVENTS_PATH}${ev.image}`;
                    ev.creator.avatar = `${IMG_PROFILE_PATH}${ev.creator.avatar}`;
                });
                return response.events;
            } else {
                return [];
            }
        }).catch(error => Observable.throw(error));
    }
    // Get my events
    getEventsMine(): Observable<IEvent[]> {

        return this.http.get(`${SERVER}events/mine`).map((response: { events: IEvent[], ok: boolean }) => {
            if (response.ok) {
                response.events.map(ev => {
                    ev.image = `${IMG_EVENTS_PATH}${ev.image}`;
                    ev.creator.avatar = `${IMG_PROFILE_PATH}${ev.creator.avatar}`;
                });
                return response.events;
            } else {
                return [];
            }
        }).catch(error => Observable.throw(error));
    }
    // Get Events I attend
    getEventsAttend(): Observable<IEvent[]> {

        return this.http.get(`${SERVER}events/attend`).map((response: { events: IEvent[], ok: boolean }) => {
            if (response.ok) {
                response.events.map(ev => {
                    ev.image = `${IMG_EVENTS_PATH}${ev.image}`;
                    ev.creator.avatar = `${IMG_PROFILE_PATH}${ev.creator.avatar}`;
                });
                return response.events;
            } else {
                return [];
            }
        }).catch(error => Observable.throw(error));
    }
    // Get a concrete event
    getEvent(id: number): Observable<IAddEvent> {
        return this.http.get(`${SERVER}events/${id}`)
            .map((resp: EventResponse) => {

                resp.event.image = `${IMG_EVENTS_PATH}${resp.event.image}`;
                resp.event.creator.avatar = `${IMG_PROFILE_PATH}${resp.event.creator.avatar}`;
                console.log(resp.event);
                return resp.event;
            }).catch(error => Observable.throw(error));
    }
    // Create a new Event
    addEvent(event: IAddEvent): Observable<boolean> {
        return this.http.post(`${SERVER}events`, event)
            .catch((resp: HttpErrorResponse) => Observable.throw('Error with add product service'))
            .map((resp: OkResponse) => {
                console.log(resp.ok);
                if (!resp.error) {
                    return true;
                }
                throw resp.errorMsg;
            });
    }
    // user buy a ticket
    attendEvent(idEvent: number, tickets: number): Observable<boolean> {
        return this.http.post(`${SERVER}events/attend/${idEvent}`, {tickets: tickets})
            .map((response: { ok: boolean, result: any, error?: string }) => {
                console.log('response: ', response.ok);
                if (response.ok) {
                    return response.ok;
                }
                throw response.error;
            });
    }
    // list of user which attend a event
    listUserAttends(idEvent: number): Observable<IUser[]> {
        return this.http.get(`${SERVER}events/attend/${idEvent}`).map((response: { result: IUser[], ok: boolean }) => {
            if (response.ok) {
                response.result.map(us => {
                    // ev.image = `${IMG_EVENTS_PATH}${ev.image}`;
                    us.avatar = `${IMG_PROFILE_PATH}${us.avatar}`;
                });
                return response.result;
            } else {
                return [];
            }
        }).catch(error => Observable.throw(error));
    }
    // update event
    updateEvent(event: IAddEvent) {
        return this.http.put(`${SERVER}events/edit/${event.id}`, event)
            .catch((error: HttpErrorResponse) => Observable.throw(`Error updating events. Server returned ${error.message}`))
            .map((response: OkResponse) => {
                console.log(response.ok);
                if (!response.error) {
                    return true;
                }
                throw response.error;
            });
    }
    // delete a event
    removeEvent(id: number): Observable<boolean> {

        return this.http.delete(`${SERVER}events/${id}`).map((response: { ok: boolean, error?: string }) => {
            if (response.ok) {
                return response.ok;
            }
            throw response.error;
        });
    }
}
