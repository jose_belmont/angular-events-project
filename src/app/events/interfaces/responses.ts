import { IEvent } from './i-event';
import { OkResponse } from '../../interfaces/responses';

export interface EventsResponse extends OkResponse {
  events: IEvent[];
}

export interface EventResponse extends OkResponse {
  event: IEvent;
}
