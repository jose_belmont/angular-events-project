export interface IAddEvent {
    id?: number;
    creator?: number;
    title: string;
    image: string;
    date: string;
    description: string;
    price: number;
    address?: string;
    distance?: number;
    lat: number;
    lng: number;
    mine?: boolean;
}
