export interface IEvent {
  id?: number;
  creator?: {
      id?: number,
      name?: string,
      email?: string,
      avatar?: string
  };
  title: string;
  image: string;
  date: string;
  description: string;
  price: number;
  address?: string;
  distance?: number;
  lat: number;
  lng: number;
  mine?: boolean;
}
