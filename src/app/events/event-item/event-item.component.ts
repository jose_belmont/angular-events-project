import { SERVER } from '../../app.constants';
import { IEvent } from '../interfaces/i-event';
import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { EventService } from '../services/event.service';
import swal from 'sweetalert2';


@Component({
  selector: 'ae-event-item',
  templateUrl: './event-item.component.html',
  styleUrls: ['./event-item.component.css']
})
export class EventItemComponent implements OnInit {
  @Input() event: IEvent;
  @Output() deleted: EventEmitter<void> = new EventEmitter();
  constructor(private eventService: EventService) { }
  ngOnInit() {
  }

  removeEvent() {
      swal({
          title: 'Are you sure?',
          text: 'You will not be able to recover this ' + this.event.title + ' event!',
          type: 'warning',
          showCancelButton: true,
          confirmButtonText: 'Yes, delete it!',
          cancelButtonText: 'No, keep it'
      }).then((result) => {
          if (result.value) {
              this.deleted.emit();
              swal(
                  'Deleted!',
                  'Your event ' + this.event.title + ' has been deleted.',
                  'success'
              );
              // result.dismiss can be 'overlay', 'cancel', 'close', 'esc', 'timer'
          } else if (result.dismiss === 'cancel') {
              swal(
                  'Cancelled',
                  'Your ' + this.event.title + ' is safe :)',
                  'error'
              );
          }
      });
  }

}
