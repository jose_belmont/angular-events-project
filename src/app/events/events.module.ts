import { UserModule } from './../user/user.module';
import { PaypalButtonModule } from './../paypal-button/paypal-button.module';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { EventsShowComponent, Show } from './events-show/events-show.component';
import { EventAddComponent } from './event-add/event-add.component';
import { EventItemComponent } from './event-item/event-item.component';
import { EventFilterPipe } from './pipes/event-filter.pipe';
import { EventDetailsComponent } from './event-details/event-details.component';
import { EventService } from './services/event.service';
import { EventResolver } from './resolvers/event.resolver';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { RouterModule } from '@angular/router';
import { SharedModule } from '../shared/shared.module';
import { AgmCoreModule } from '@agm/core';
import { EditEventDeactivateGuard } from './guards/edit-event-deactivate.guard';


@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    RouterModule.forChild([
      {
        path: '',
        component: EventsShowComponent,
        data: {show: Show.all}
      },
      {
        path: 'add',
        component: EventAddComponent,
      },
      {
        path: 'edit/:id',
        component: EventAddComponent,
        canDeactivate: [EditEventDeactivateGuard],
          resolve: {
              event: EventResolver
          }
      },
      {
        path: 'mine',
        component: EventsShowComponent,
        data: {show: Show.mine}
      },
      {
        path: 'attend',
        component: EventsShowComponent,
         data: {show: Show.assist}
      },
      {
        path: ':id',
        component: EventDetailsComponent,
        resolve: {
          event: EventResolver
        }
      }
    ]),
    SharedModule,
    AgmCoreModule.forRoot({
        apiKey: 'AIzaSyC2sYQIcXgK0jbglbKQHc_ImsBieJwohHQ',
        libraries: ['places']
    }),
    PaypalButtonModule.forRoot({
        sandbox: 'Acwd372rNIs9Hzo_sac6Y4sxKvtBG59DA-zmSbyvD9rRqsp8LePlTVKKY3AYp3mIUNjJjlhLgmSSzGq_',
        production: '',
        environment: 'sandbox'
    })
  ],
  declarations: [
    EventsShowComponent,
    EventAddComponent,
    EventItemComponent,
    EventFilterPipe,
    EventDetailsComponent,
  ],
  providers: [
    EventService,
    EventResolver,
    EditEventDeactivateGuard
  ]
})
export class EventsModule { }
