import { TestBed, async, inject } from '@angular/core/testing';

import { EditEventDeactivateGuard } from './edit-event-deactivate.guard';

describe('EditEventDeactivateGuard', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [EditEventDeactivateGuard]
    });
  });

  it('should ...', inject([EditEventDeactivateGuard], (guard: EditEventDeactivateGuard) => {
    expect(guard).toBeTruthy();
  }));
});
