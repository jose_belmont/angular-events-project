import { NgbModal } from '@ng-bootstrap/ng-bootstrap/modal/modal';
import { Injectable } from '@angular/core';
import { CanDeactivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/of';
import 'rxjs/add/observable/fromPromise';
import 'rxjs/add/operator/switchMap';
import { EventAddComponent } from '../event-add/event-add.component';
import { EventService } from '../services/event.service';
import swal from 'sweetalert2';
import { ConfirmModalComponent } from '../../shared/confirm-modal/confirm-modal.component';


@Injectable()
export class EditEventDeactivateGuard implements CanDeactivate<EventAddComponent> {

    constructor(private eventService: EventService, private router: Router, private ngbModal: NgbModal) { }

    canDeactivate(
        component: EventAddComponent,
        currentRoute: ActivatedRouteSnapshot,
        currentState: RouterStateSnapshot,
        nextState?: RouterStateSnapshot): Observable<boolean> {

        if (!component.isSave) {
            const modalRef = this.ngbModal.open(ConfirmModalComponent);
            modalRef.componentInstance.title = 'Save changes';
            modalRef.componentInstance.body = ['Do you want to save changes?'];

            console.log(modalRef.result);
            return Observable.fromPromise(modalRef.result)
                .switchMap(ok => {
                    if (ok) {
                        return this.eventService.updateEvent(component.newEvent)
                            .map(event => true)
                            .catch(error => {
                                return Observable.of(false);
                            });
                    } else {
                        return Observable.of(true);
                    }
                })
                .catch((error) => {
                    console.log(error);
                    return Observable.of(false);
                });
        }
        /*if (!component.isSave) {
            swal({
                title: 'Are you sure?',
                text: 'Do you want save the event before exit?!',
                type: 'warning',
                showCancelButton: true,
                confirmButtonText: 'Yes, exit it!',
                cancelButtonText: 'No, stay here'
            }).then((result) => {
                if (result.value) {
                    swal(
                        'Saved!',
                        'This event has update',
                        'success'
                    );

                    return this.eventService.updateEvent(component.newEvent)
                        .map(event => true)
                        .catch(error => {
                            return Observable.of(false);
                        });
                    // result.dismiss can be 'overlay', 'cancel', 'close', 'esc', 'timer'
                } else if (result.dismiss === 'cancel') {
                    swal(
                        'Cancelled',
                        'Your will stay in the page)',
                        'error'
                    );
                    //this.router.navigate(['/events/edit', currentRoute.params['id']]);
                    return Observable.of(false);
                }
            });
            return Observable.of(true);
        }
        else {
            return Observable.of(true);
        }*/
    }

}



