import { IEvent } from '../interfaces/i-event';
import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'eventFilter'
})
export class EventFilterPipe implements PipeTransform {

  transform(events: IEvent[], search: string): IEvent[] {
     const s = search.trim().toLocaleLowerCase();
     return s ? events.filter(e => e.title.toLocaleLowerCase().includes(s)) : events;
  }

}
