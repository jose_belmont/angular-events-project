import { SharedModule } from './../shared/shared.module';
import { GeolacationService } from './services/geolacation.service';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LoginComponent } from './login/login.component';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { RegisterComponent } from './register/register.component';
import { UserService } from './services/user.service';
import { GoogleLoginModule } from './google-login/google-login.module';
import { FacebookLoginModule } from './facebook-login/facebook-login.module';

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        SharedModule,
        GoogleLoginModule.forRoot('422674547923-actepnjk0mjeg73lq7buv5hv1be063ro.apps.googleusercontent.com'),
        FacebookLoginModule.forRoot({ app_id: '419401718491410', version: 'v2.11' }),
        RouterModule.forChild([
            {
                path: 'login',
                component: LoginComponent,
            },
            {
                path: 'register',
                component: RegisterComponent,
            }
        ])
    ],
    declarations: [
        LoginComponent,
        RegisterComponent
    ],
    providers: [
        UserService,
        GeolacationService
    ]
})
export class AuthModule { }
