import { UserService } from './../services/user.service';
import { IUser } from './../interfaces/i-user';
import { Router } from '@angular/router';
import { Component, OnInit, NgZone } from '@angular/core';
import { AuthService } from '../../services/auth.service';
import { GeolacationService } from '../services/geolacation.service';
import { LoadGoogleApiService } from '../google-login/services/load-google-api.service';

@Component({
    selector: 'ae-login',
    templateUrl: './login.component.html',
    styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
    error = false;
    email = '';
    password = '';
    errorMsg: string;
    user: IUser;

    constructor(
        private authService: AuthService,
        private router: Router,
        private geolocation: GeolacationService,

    ) { }

    ngOnInit() {
        this.createCoords();
        this.resetNewUser();
    }

    resetNewUser() {
        this.user = {
            name: '',
            email: '',
            password: '',
            avatar: ''
        };
    }
    createCoords() {
        this.geolocation.getLocation().subscribe(
            position => {
                this.user.lat = position.coords.latitude;
                this.user.lng = position.coords.longitude;
            },
            error => console.log(error),
        );
    }

    loggedGoogle(user: gapi.auth2.GoogleUser) {
        const token = user.getAuthResponse(true).access_token;
        this.user.email = user.getBasicProfile().getEmail();
        this.user.avatar = user.getBasicProfile().getImageUrl();
        this.user.name = user.getBasicProfile().getName();

        this.authService.loginGoogle(this.user, token).subscribe(
            result => this.router.navigate(['/events']),
            error => this.error = error
        );
    }
    loggedFacebook(user: FB.LoginStatusResponse) {
        const token = user.authResponse.accessToken;
        this.authService.loginFacebook(this.user, token).subscribe(
            result => this.router.navigate(['/events']),
            error => this.error = error
        );
    }
    login() {
        this.authService.login(this.email, this.password, this.user.lat, this.user.lng).
            subscribe(value => {
                if (value) {
                    this.router.navigate(['/events']);
                } else {
                    this.error = true;
                }
            });
    }

}
