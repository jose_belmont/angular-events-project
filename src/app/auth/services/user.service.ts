import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { IUser } from '../interfaces/i-user';
import { Observable } from 'rxjs/Observable';
import { OkResponse } from '../../interfaces/responses';
import { SERVER } from '../../app.constants';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';

@Injectable()
export class UserService {

  constructor(private http: HttpClient) { }

  addUser(user: IUser): Observable<boolean> {
    return this.http.post(`${SERVER}auth/register`, user)
      .catch((resp: HttpErrorResponse) => Observable.throw('Error with add user service'))
      .map((resp: OkResponse) => {
        if (!resp.ok) {
          throw resp.errors;
        }
        return true;
      });
  }
}
