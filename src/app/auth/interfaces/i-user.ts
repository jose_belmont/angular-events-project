export interface IUser {
  id?: number;
  id_google?: number;
  id_facebook?: number;
  name: string;
  email: string;
  password: string;
  avatar: string;
  lat?: number;
  lng?: number;
  mine?: boolean;
}
