import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { IUser } from '../interfaces/i-user';
import { Router } from '@angular/router';
import { NgModel } from '@angular/forms';
import { UserService } from '../services/user.service';
import { GeolacationService } from '../services/geolacation.service';


@Component({
    selector: 'ae-register',
    templateUrl: './register.component.html',
    styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {

    @Output() userAdded = new EventEmitter<IUser>();
    newUser: IUser;
    error = false;
    errors = [];



    constructor(private router: Router,
        private userService: UserService, private geolocation: GeolacationService) { }

    ngOnInit() {
        this.geolocation.getLocation().subscribe(
            position => {
                this.newUser.lat = position.coords.latitude;
                this.newUser.lng = position.coords.longitude;
            },

            error => console.log(error),
        );
        this.resetNewUser();


    }

    resetNewUser() {
        this.newUser = {
            name: '',
            email: '',
            password: '',
            avatar: ''
        };
    }

    changeImage(fileInput: HTMLInputElement) {
        if (!fileInput.files || fileInput.files.length === 0) { return; }
        const reader: FileReader = new FileReader();
        reader.readAsDataURL(fileInput.files[0]);
        reader.addEventListener('loadend', e => {
            this.newUser.avatar = reader.result;
        });
    }

    addUser() {
        this.userService.addUser(this.newUser).subscribe(response => {
            if (response) {
                this.router.navigate(['/auth/login']);
            }
        }, error => {
            this.errors = error;
            this.error = true;
        });

    }

    validateClasses(ngModel: NgModel, validClass: string, errorClass: string) {
        return {
            [validClass]: ngModel.touched && ngModel.valid,
            [errorClass]: ngModel.touched && ngModel.invalid
        };
    }

}

