import { Route } from '@angular/router';
import { LoginActivateGuard } from './guards/login-activate.guard';
import { LogoutActivateGuard } from './guards/logout-activate.guard';

export const APP_ROUTES: Route[] = [
  {
    path: 'auth',
    loadChildren: './auth/auth.module#AuthModule',
    canActivate: [LogoutActivateGuard]
  },
  {
    path: 'profile',
    loadChildren: './user/user.module#UserModule',
    canActivate: [LoginActivateGuard]
  },
  {
    path: 'events',
    loadChildren: './events/events.module#EventsModule',
    canActivate: [LoginActivateGuard]
  },
  // Default route (empty) -> Redirect to welcome page
  { path: '', redirectTo: '/auth/login', pathMatch: 'full' },
  // Doesn't match any of the above
  { path: '**', redirectTo: '/auth/login' }
];
