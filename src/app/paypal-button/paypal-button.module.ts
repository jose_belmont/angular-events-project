import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PaypalButtonComponent } from './paypal-button.component';
import { PaypalConfig, PAYPAL_CONFIG } from './paypal-button.config';
import { ModuleWithProviders } from '@angular/compiler/src/core';
import { PaypalLoadService } from './services/paypal-load.service';

@NgModule({
    imports: [
        CommonModule
    ],
    declarations: [PaypalButtonComponent],
    exports: [PaypalButtonComponent],
    providers: []
})
export class PaypalButtonModule {
    static forRoot(paypal_config: PaypalConfig): ModuleWithProviders {
        return {
            ngModule: PaypalButtonModule,
            providers: [
                PaypalLoadService,
                { provide: PAYPAL_CONFIG, useValue: paypal_config }
            ]
        };
    }
}
