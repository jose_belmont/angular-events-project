import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { AuthService } from '../services/auth.service';

@Injectable()
export class LogoutActivateGuard implements CanActivate {
  constructor(private authService: AuthService, private router: Router) {}

    canActivate(
      next: ActivatedRouteSnapshot,
      state: RouterStateSnapshot): Observable<boolean> {
      return this.authService.isLogged()
        .map(ok => {
          if (ok) { // User is logged (redirect to events)
            this.router.navigate(['events']);
          }
          return !ok;
        })
        .catch(error => Observable.of(true)); // Ok -> Not logged in;
    }
}
