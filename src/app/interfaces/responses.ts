import { IEvent } from './../events/interfaces/i-event';
import { IUser } from './../auth/interfaces/i-user';


export interface OkResponse {
    ok: boolean;
    error?: string;
    errors?: string[];
    errorMsg?: string;
    errorMessage?: string;
    token?: string;
    user?: IUser;
    users?: IUser[];
    avatar?: string;
    event?: IEvent;
    events?: IEvent[];
    image?: string;
    result?: any;
}

