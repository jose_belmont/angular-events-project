import { IMG_PROFILE_PATH } from './../../app.constants';
import { OkResponse } from './../../interfaces/responses';
import { IUser } from './../../auth/interfaces/i-user';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { SERVER } from '../../app.constants';

@Injectable()
export class ProfileService {

    constructor(private http: HttpClient) { }
    // Get a concrete user
    getUser(id: number): Observable<IUser> {
        return this.http.get(`${SERVER}profile/${id}`)
            .catch(error => Observable.throw('Error with getting profile, because:  ' + error))
            .map((resp: OkResponse) => {
                if (!resp.error) {
                    resp.user.avatar = `${IMG_PROFILE_PATH}${resp.user.avatar}`;
                    return resp.user;
                }
                throw resp.errorMessage;
            });
    }

    // Get my profile
    getMyProfile(): Observable<IUser> {
        return this.http.get(`${SERVER}profile`)
            .catch(error => Observable.throw('Error with getting my profile, because:  ' + error))
            .map((resp: OkResponse) => {
                if (!resp.error) {
                    resp.user[0].avatar = `${IMG_PROFILE_PATH}${resp.user[0].avatar}`;
                    resp.user[0].mine = true;
                    console.log('objeto:', resp.user);
                    return resp.user[0];
                }
                throw resp.errorMessage;
            });
    }

    changeUser(user: IUser): Observable<boolean> {
        const data = {
            name: user.name,
            email: user.email
        };

        return this.http.put(`${SERVER}users/me`, data)
            .map((resp: { ok: boolean, error?: string }) => {
                if (resp.ok) {
                    return resp.ok;
                }
                throw resp.error;
            });
    }

    changePassword(data): Observable<boolean> {
        return this.http.put(`${SERVER}users/me/password`, data)
            .map((resp: { ok: boolean, error?: string }) => {
                if (resp.ok) {
                    return resp.ok;
                }
                throw resp.error;
            });
    }

    changeAvatar(user: IUser): Observable<boolean> {
        const data = {
            avatar: user.avatar
        };

        return this.http.put(`${SERVER}users/me/avatar`, data)
            .map((resp: { ok: boolean, error?: string }) => {
                if (resp.ok) {
                    return resp.ok;
                }
                throw resp.error;
            });
    }


}
