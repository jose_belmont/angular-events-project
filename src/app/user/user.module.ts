import { ProfileService } from './service/profile.service';
import { ProfileResolverService } from './resolver/profile-resolver.service';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ProfileComponent } from './profile/profile.component';
import { ProfileEditComponent } from './profile-edit/profile-edit.component';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { SharedModule } from '../shared/shared.module';
import { AuthModule } from '../auth/auth.module';
import { AgmCoreModule } from '@agm/core';

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        RouterModule.forChild([

            {
                path: '',
                component: ProfileComponent,
                resolve: {
                    profile: ProfileResolverService,
                },
            },
            {
                path: 'edit/:id',
                component: ProfileEditComponent,
                resolve: {
                    profile: ProfileResolverService,
                },
            },
            {
                path: ':id',
                component: ProfileComponent,
                resolve: {
                    profile: ProfileResolverService,
                },
            },

        ]),
        SharedModule,
        AgmCoreModule.forRoot({
            apiKey: 'AIzaSyC2sYQIcXgK0jbglbKQHc_ImsBieJwohHQ',
            libraries: ['places']
        }),
    ],
    declarations: [ProfileComponent, ProfileEditComponent],
    providers: [ProfileResolverService, ProfileService]
})
export class UserModule { }
