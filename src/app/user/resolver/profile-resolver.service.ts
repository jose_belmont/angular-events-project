import { UserService } from './../../auth/services/user.service';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/of';
import { Resolve, Router, ActivatedRouteSnapshot } from '@angular/router';
import { ProfileService } from '../service/profile.service';
import { IUser } from '../../auth/interfaces/i-user';

@Injectable()
export class ProfileResolverService implements Resolve<IUser> {

    constructor(private profileService: ProfileService, private router: Router) { }
    resolve(route: ActivatedRouteSnapshot): Observable<IUser> {
        if (route.params['id']) {
            return this.profileService.getUser(route.params['id']).catch(err => {
                this.router.navigate(['/profile', route.params['id']]);
                return Observable.of(null);
            });
        } else {
            return this.profileService.getMyProfile().catch(err => {
                this.router.navigate(['/profile', route.params['id']]);
                return Observable.of(null);
            });
        }
    }
}
