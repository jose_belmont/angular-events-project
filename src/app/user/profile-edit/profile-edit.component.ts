import { ProfileService } from './../service/profile.service';
import { IUser } from './../../auth/interfaces/i-user';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import swal from 'sweetalert2';


@Component({
    selector: 'ae-profile-edit',
    templateUrl: './profile-edit.component.html',
    styleUrls: ['./profile-edit.component.css']
})
export class ProfileEditComponent implements OnInit {
    user: IUser;
    avatar: any;
    password: string;
    error: string;
    success = false;
    fileIsValid: boolean;

    constructor(private route: ActivatedRoute,
        private profileService: ProfileService,
        private router: Router
    ) { }

    ngOnInit() {
        this.user = this.route.snapshot.data['profile'];
    }
    confirmation() {
        if (this.success === true) {
        return swal (
            'Good Job', 'this update is succefull', 'success');
        } else {
            return swal(
                'We have a problem', 'A error ocurred with update', 'error');
        }
    }

    changeImage(fileInput: HTMLInputElement) {
        if (!fileInput.files || fileInput.files.length === 0) {
            return;
        }

        if (!fileInput.files[0].type.startsWith('image/')) {
            this.success = false;
            this.fileIsValid = false;
            return;
        } else {
            this.fileIsValid = true;
        }

        const reader: FileReader = new FileReader();
        console.log(fileInput.files[0].type);
        reader.readAsDataURL(fileInput.files[0]);
        reader.addEventListener('loadend', e => {
            this.user.avatar = reader.result;
        });
    }

    changeUser() {
        this.profileService.changeUser(this.user)
            .subscribe(resp => {
                this.success = true;
            },
            error => this.error);
    }

    changeAvatar() {
        if (!this.fileIsValid) {
            return;
        }
        this.success = false;

        this.profileService.changeAvatar(this.user)
            .subscribe(resp => {
                this.success = true;
            },
            error => this.error);
    }

    changePassword() {
        this.success = false;

        this.profileService.changePassword(this.password)
            .subscribe(resp => {
                this.success = true;
            },
            error => this.error);
    }

}
