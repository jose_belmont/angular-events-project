import { ProfileService } from './../service/profile.service';

import { IUser } from './../../auth/interfaces/i-user';
import { Component, OnInit, Input } from '@angular/core';
import { ActivatedRoute, ActivatedRouteSnapshot } from '@angular/router';

@Component({
    selector: 'ae-profile',
    templateUrl: './profile.component.html',
    styleUrls: ['./profile.component.css']
})
export class ProfileComponent implements OnInit {
    user: IUser;
    zoom = 17;
    constructor(private route: ActivatedRoute, private profileService: ProfileService) { }

    ngOnInit() {
        this.user = this.route.snapshot.data['profile'];
    }

}
